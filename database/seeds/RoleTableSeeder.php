<?php

use estadia\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->nombre = 'admi';
        $role->save();
        $role = new Role();
        $role->nombre = 'user';
        $role->save();

        }
}
