@extends('layouts.app')

@section('content')
<div class="col-sm-7 col-center">
	<div class="card text-center" style="width: 18rem;margin-top: 50px">
	<img class="card-img-top rounded-circle mx-auto d-block" src="/images/{{ $servicio->img_url }}" alt="" style="height: 200px; background-color:#EFE; margin: 30px">
	
	<div class="card-body">
		<h10 >{{$servicio->nombre}}</h10>
		<h5> {{$servicio->posicion}} </h5>
	</div>
</div>
<form method="POST" action="/servicio/{{$servicio->nombre}}">
	{{method_field('DELETE')}}
	{{ csrf_field() }}

	<button type="submit" class="btn btn-success">Eliminar</button>
</form>
</div>
@endsection