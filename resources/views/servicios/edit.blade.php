@extends('layouts.app')

@section('content')
<div class="container mt-3">
	<form class="form-group" method="POST" action="/servicios/{{$servicio->nombre}}" enctype="multipart/form-data">
		{{ csrf_field() }}

		{{method_field('put')}}
		
		<div class="form-group">
			
			<label for="">nombre</label>
			<input type="text" class="form-control" name="nombre" value="{{$servicio->nombre}}">
		</div>
		<div class="form-group">

			<label for="">posicion</label>
			<input type="text" name="posicion" class="form-control" value="{{$servicio->posicion}}">
		</div>
		<div class="form-group">
			<label for="">Avatar</label>
			<input type="file" name="avatar">
		</div>
		<br>
		<button type="submit" class="btn btn-success">actualizar</button>
	</form>
@endsection