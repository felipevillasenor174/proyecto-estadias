@extends('layouts.app')

@section('content')
<div class="container mt-3">
	<form class="form-group" method="POST" action="/servicios" enctype="multipart/form-data">
		<div class="form-group">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<label for="">nombre</label>
			<input type="text" class="form-control" name="nombre">
		</div>
		<div class="form-group">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<label for="">posicion</label>
			<input type="text" name="posicion" class="form-control">
		</div>
		<div class="form-group">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<label for="">Avatar</label>
			<input type="file" name="avatar">
		</div>
		<br>
		<button type="submit" class="btn btn-success">Guardar</button>
	</form>
@endsection