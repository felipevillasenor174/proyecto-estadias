@extends('layouts.app')

@section('title', 'servicio')

@section('content')

<div class="row">
	
@foreach($servicios as $row)
@if(session('status'))
<div class="alert alert-success" >
	{{session('status') }}
</div>
@endif
<div class="col-sm-5">
	<div class="card text-center" style="width: 18rem;margin-top: 50px">
		<img class="card-img-top rounded-circle mx-auto d-block" src="images/{{ $row->img_url }}" alt="" style="height: 100px; background-color:#EFE; margin: 30px">
	
	<div class="card-body">
		<h5 class="card-title">{{$row->nombre}}</h5>
		<a href="/servicios/{{$row->nombre}}/edit" class="btn btn-primary">editar</a>
		<a href="/servicios/{{$row->nombre}}" class="btn btn-primary">eliminar</a>

	</div>
</div>
</div>
@endforeach
</div>
@endsection