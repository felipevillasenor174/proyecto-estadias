
@extends('plataforma.layout')
@section('title')Estadia_Charli @stop
@section('content')
<div class="col-lg-12">
    <section class="box ">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="r4_counter db_box">
                    <i class="pull-left fa fa-thumbs-up icon-md icon-rounded icon-primary"></i>
                    <div class="stats">
                        <h4><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">45%</font></font></strong></h4>
                        <span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nuevos pedidos</font></font></span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="r4_counter db_box">
                    <i class="pull-left fa fa-shopping-cart icon-md icon-rounded icon-orange"></i>
                    <div class="stats">
                        <h4><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">243</font></font></strong></h4>
                        <span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">nuevos productos</font></font></span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="r4_counter db_box">
                    <i class="pull-left fa fa-dollar icon-md icon-rounded icon-purple"></i>
                    <div class="stats">
                        <h4><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">$ 3424</font></font></strong></h4>
                        <span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Beneficio Hoy</font></font></span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="r4_counter db_box">
                    <i class="pull-left fa fa-users icon-md icon-rounded icon-warning"></i>
                    <div class="stats">
                        <h4><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1433</font></font></strong></h4>
                        <span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Usuarios nuevos</font></font></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5 col-sm-12 col-xs-12">
                <div class="r3_notification db_box">
                    <h4><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Notificaciones</font></font></h4>

                    <ul class="list-unstyled notification-widget ps-container ps-active-y" style="height: 315px;">


                        <li class="unread status-available">
                            <a href="javascript:;">
                                <div class="user-img">
                                    <img src="data/profile/avatar-1.png" alt="imagen de usuario" class="img-circle img-inline">
                                </div>
                                <div>
                                    <span class="name">
                                        <strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Clarine Vassar </font></font></strong>
                                        <span class="time small"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">- Hace 15 minutos</font></font></span>
                                        <span class="profile-status available pull-right"></span>
                                    </span>
                                    <span class="desc small"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                                        A veces lleva toda una vida ganar una batalla.
                                    </font></font></span>
                                </div>
                            </a>
                        </li>


                        <li class=" status-away">
                            <a href="javascript:;">
                                <div class="user-img">
                                    <img src="data/profile/avatar-2.png" alt="imagen de usuario" class="img-circle img-inline">
                                </div>
                                <div>
                                    <span class="name">
                                        <strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Brooks Latshaw </font></font></strong>
                                        <span class="time small"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">- Hace 45 minutos</font></font></span>
                                        <span class="profile-status away pull-right"></span>
                                    </span>
                                    <span class="desc small"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                                        A veces lleva toda una vida ganar una batalla.
                                    </font></font></span>
                                </div>
                            </a>
                        </li>


                        <li class=" status-busy">
                            <a href="javascript:;">
                                <div class="user-img">
                                    <img src="data/profile/avatar-3.png" alt="imagen de usuario" class="img-circle img-inline">
                                </div>
                                <div>
                                    <span class="name">
                                        <strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Clementina Brodeur </font></font></strong>
                                        <span class="time small"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">- Hace 1 hora</font></font></span>
                                        <span class="profile-status busy pull-right"></span>
                                    </span>
                                    <span class="desc small"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                                        A veces lleva toda una vida ganar una batalla.
                                    </font></font></span>
                                </div>
                            </a>
                        </li>


                        <li class=" status-offline">
                            <a href="javascript:;">
                                <div class="user-img">
                                    <img src="data/profile/avatar-4.png" alt="imagen de usuario" class="img-circle img-inline">
                                </div>
                                <div>
                                    <span class="name">
                                        <strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Carri Busey </font></font></strong>
                                        <span class="time small"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">- Hace 5 horas</font></font></span>
                                        <span class="profile-status offline pull-right"></span>
                                    </span>
                                    <span class="desc small"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                                        A veces lleva toda una vida ganar una batalla.
                                    </font></font></span>
                                </div>
                            </a>
                        </li>


                        <li class=" status-offline">
                            <a href="javascript:;">
                                <div class="user-img">
                                    <img src="data/profile/avatar-5.png" alt="imagen de usuario" class="img-circle img-inline">
                                </div>
                                <div>
                                    <span class="name">
                                        <strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Melissa Dock </font></font></strong>
                                        <span class="time small"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">- Ayer</font></font></span>
                                        <span class="profile-status offline pull-right"></span>
                                    </span>
                                    <span class="desc small"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                                        A veces lleva toda una vida ganar una batalla.
                                    </font></font></span>
                                </div>
                            </a>
                        </li>


                        <li class=" status-available">
                            <a href="javascript:;">
                                <div class="user-img">
                                    <img src="data/profile/avatar-1.png" alt="imagen de usuario" class="img-circle img-inline">
                                </div>
                                <div>
                                    <span class="name">
                                        <strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Verdell Rea </font></font></strong>
                                        <span class="time small"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">- 14 de marzo</font></font></span>
                                        <span class="profile-status available pull-right"></span>
                                    </span>
                                    <span class="desc small"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                                        A veces lleva toda una vida ganar una batalla.
                                    </font></font></span>
                                </div>
                            </a>
                        </li>


                        <li class=" status-busy">
                            <a href="javascript:;">
                                <div class="user-img">
                                    <img src="data/profile/avatar-2.png" alt="imagen de usuario" class="img-circle img-inline">
                                </div>
                                <div>
                                    <span class="name">
                                        <strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Linette Lheureux </font></font></strong>
                                        <span class="time small"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">- 16 de marzo</font></font></span>
                                        <span class="profile-status busy pull-right"></span>
                                    </span>
                                    <span class="desc small"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                                        A veces lleva toda una vida ganar una batalla.
                                    </font></font></span>
                                </div>
                            </a>
                        </li>


                        <li class=" status-away">
                            <a href="javascript:;">
                                <div class="user-img">
                                    <img src="data/profile/avatar-3.png" alt="imagen de usuario" class="img-circle img-inline">
                                </div>
                                <div>
                                    <span class="name">
                                        <strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Araceli Boatright </font></font></strong>
                                        <span class="time small"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">- 16 de marzo</font></font></span>
                                        <span class="profile-status away pull-right"></span>
                                    </span>
                                    <span class="desc small"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                                        A veces lleva toda una vida ganar una batalla.
                                    </font></font></span>
                                </div>
                            </a>
                        </li>


                        <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 315px; right: 3px;"><div class="ps-scrollbar-y" style="top: 0px; height: 134px;"></div></div></ul>

                    </div>
                </div>      

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="r3_weather">
                        <div class="wid-weather wid-weather-small">
                            <div class="">

                                <div class="location">
                                    <h3><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">California, EE.UU</font></font></h3>
                                    <span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Hoy en día, el 12 </font></font><sup><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">º</font></font></sup><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> de marzo de el año 2015</font></font></span>
                                </div>
                                <div class="clearfix"></div>
                                <div class="degree">
                                    <i class="fa fa-cloud icon-lg text-white"></i><span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ahora</font></font></span><h3><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">24 °</font></font></h3>
                                    <div class="clearfix"></div>
                                    <h4 class="text-white text-center"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Caluroso y soleado</font></font></h4>
                                </div>
                                <div class="clearfix"></div>
                                <div class="weekdays bg-white">
                                    <ul class="list-unstyled ps-container ps-active-y">
                                        <li><span class="day"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">domingo</font></font></span><i class="fa fa-cloud icon-xs"></i><span class="temp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">23 ° - 27 °</font></font></span></li>
                                        <li><span class="day"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">lunes</font></font></span><i class="fa fa-cloud icon-xs"></i><span class="temp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">21 ° - 26 °</font></font></span></li>
                                        <li><span class="day"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">martes</font></font></span><i class="fa fa-cloud icon-xs"></i><span class="temp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">24 ° - 28 °</font></font></span></li>
                                        <li><span class="day"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">miércoles</font></font></span><i class="fa fa-cloud icon-xs"></i><span class="temp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">25 ° - 26 °</font></font></span></li>
                                        <li><span class="day"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">jueves</font></font></span><i class="fa fa-cloud icon-xs"></i><span class="temp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">22 ° - 25 °</font></font></span></li>
                                        <li><span class="day"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">viernes</font></font></span><i class="fa fa-cloud icon-xs"></i><span class="temp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">21 ° - 28 °</font></font></span></li>
                                        <li><span class="day"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Sabado</font></font></span><i class="fa fa-cloud icon-xs"></i><span class="temp"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">23 ° - 29 °</font></font></span></li>
                                        <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 175px; right: 3px;"><div class="ps-scrollbar-y" style="top: 0px; height: 99px;"></div></div></ul>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>      

                    <div class="col-md-4 col-sm-6 col-xs-12">

                        <div class="ultra-widget ultra-todo-task bg-primary">
                            <div class="wid-task-header">
                                <div class="wid-icon">
                                    <i class="fa fa-tasks"></i>
                                </div>
                                <div class="wid-text">
                                    <h4><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Hacer tareas</font></font></h4>
                                    <span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Miér </font></font><small><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">11 </font></font><sup><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">º</font></font></sup><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> de marzo de el año 2015</font></font></small></span>
                                </div>
                            </div>
                            <div class="wid-all-tasks">

                                <ul class="list-unstyled ps-container">

                                    <li class="checked">
                                        <div class="icheckbox_minimal-white checked" style="position: relative;"><input type="checkbox" id="task-1" class="icheck-minimal-white todo-task" checked="" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                        <label class="icheck-label form-label" for="task-1"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Proyectos de oficina</font></font></label>
                                    </li> 
                                    <li>
                                        <div class="icheckbox_minimal-white" style="position: relative;"><input type="checkbox" id="task-2" class="icheck-minimal-white todo-task" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                        <label class="icheck-label form-label" for="task-2"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Generar factura</font></font></label>
                                    </li>  

                                    <li>
                                        <div class="icheckbox_minimal-white" style="position: relative;"><input type="checkbox" id="task-3" class="icheck-minimal-white todo-task" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                        <label class="icheck-label form-label" for="task-3"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Tema de comercio electrónico</font></font></label>
                                    </li> 
                                    <li>
                                        <div class="icheckbox_minimal-white" style="position: relative;"><input type="checkbox" id="task-4" class="icheck-minimal-white todo-task" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                        <label class="icheck-label form-label" for="task-4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PHP y jQuery</font></font></label>
                                    </li> 
                                    <li>
                                        <div class="icheckbox_minimal-white" style="position: relative;"><input type="checkbox" id="task-5" class="icheck-minimal-white todo-task" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                                        <label class="icheck-label form-label" for="task-5"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Asignar recurso</font></font></label>
                                    </li> 
                                    <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px;"><div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div></div></ul>

                                </div>
                                <div class="wid-add-task">
                                    <input type="text" class="form-control" placeholder="Add task">
                                </div>
                            </div>


                        </div>      

                    </div>

                </section>
            </div>
            @endsection