@extends('plataforma.layout')
@section('title')Estadia_Charli @stop
@section('content')
<div class="col-md-12">
	<section class="box ">
		<header class="panel_header">
			<h2 class="title pull-left">Guardar Usuarios</h2>
			<div class="actions panel_actions pull-right">
				{{ Html::link('/usuario/create', 'Crear Nuevo', array('class' => 'btn btn-info')) }}
			</div>
		</header>
		<div class="content-body">



			<div class="row">
				@if ($us->count())
				<table class="table table table-striped dt-responsive display" id="example-1">
					<thead>
						<tr>
							<th class="text-left">Nombre</th>
							<!-- <th class="text-left">Descripción</th> -->
							<!-- <th class="text-left">Redirigir</th> -->
							<th class="text-left">Correo</th>
							<th class="text-left">Rol</th>
							<th class="text-left">Opciones</th>
						</tr>
					</thead>
					<tbody>
						@foreach($us as $row)
						<tr>
							<td width="30%" class="text-uppercase">{{ $row->name }}</td>
							<td width="15%" class="text-left">{{$row->email}}</td>
							<td width="20%" class="text-left">{{$row->rol->nombre}}
							</td>
							<td width="25%" class="text-left">
								<a href="/usuario/{{$row->id}}/edit" class="btn btn-info btn-xs pull-left right15" rel="tooltip" data-animate=" animated bounce" data-toggle="tooltip" data-original-title="Editar registro" data-placement="top">
									<i class="fa fa-pencil" ></i>
								</a>
								{{ Form::open(array('url' => 'usuario/'.$row->id)) }}
								{{ Form::hidden("_method", "DELETE") }}
								{{ Form::submit("x", array('class' => 'btn btn-xs btn-danger pull-left right15', 'onclick' => 'return confirm("Seguro que deseas eliminar?");')) }}
								{{ Form::close() }}
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				@else
				<p>No hay registros disponibles</p>
				@endif
			</div>
		</div>
	</section>
</div>


@stop
