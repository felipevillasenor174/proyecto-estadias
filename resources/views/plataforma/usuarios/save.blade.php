@extends('plataforma.layout')
@section('title') Estadia_Charli @stop
@section('content')


<section class="box">
	
	{{-- {!! Form::model($us,['route' => 'usuario.update', $us ], 'method' => 'PUT') !!} --}}

	{{-- {!!Form::close()!!} --}}
	{!!Form::open(['url' => 'usuario/'.$us2->id, 'method' => 'PHOST',  'class' => 'datosPedido'])!!}

	<div class="col-md-6">

		<section class="box ">
			<header class="panel_header">
				<h2 class="title pull-left">USUARIO</h2>

			</header>
			<div class="content-body">

				<div class="row">
					<div class="form-group">

						{!!Form::label('Name', 'Nombre') !!}
						<div class="controls">
							{!! Form::hidden('name', $us2->name) !!}
							{!! Form::text('name', $us2->name, ['class' => 'form-control titulo' ,'required' => 'required']) !!}
							@if($errors->first('name'))
							<div class="alert alert-error alert-dismissible fade in">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">×</span>
								</button>
								{{ $errors->first('name') }}
							</div>
							@endif
						</div>
					</div>       
					<div class="form-group">

						{!!Form::label('Correo', 'Correo') !!}
						<div class="controls">
							{!! Form::hidden('email', $us2->email) !!}
							{!! Form::text('email', $us2->email, ['class' => 'form-control titulo','required' => 'required']) !!}
							@if($errors->first('email'))
							<div class="alert alert-error alert-dismissible fade in">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">×</span>
								</button>
								{{ $errors->first('email') }}
							</div>
							@endif
						</div>
					</div>



					<div class="form-group">

						{!!Form::label('Contraseña', 'Nueva contraseña') !!}
						<div class="controls">
							{!! Form::hidden('password', $us2->password) !!}
							{!! Form::text('password', null, ['class' => 'form-control titulo','required' => 'required']) !!}
						</div>
					</div>



						{{-- {!!Form::label('Rol', 'Rol') !!}
						<div class="controls">
							{!! Form::hidden('id_rol', $us2->id_rol) !!}
							{!! Form::text('id_rol', $us2->id_rol, ['class' => 'form-control titulo','required' => 'required']) !!} --}}
							<div class="form-group">
								
								{{ Form::label ('id_rol', 'Rol', ['class' => 'form-label']) }}
								<div class="controls">
									{{ Form::hidden('id_rol', null) }}
									
									{{-- @if($us2->id) --}}

									{{ Form::select('id_rol', $rol,  null,  ['class' => 'rol', 'id' =>'s2example-2']) }}
									{{-- @else --}}
									{{-- {{ Form::select('id_rol',  ['id' => $row->nombre]) }} --}}

									{{-- @endif --}}
								
								</div>
								
							</div>
										




					@if($us2->id)
					{{ Form::hidden ('_method', 'PUT', ['id' => 'methodo']) }}
					@endif

					<div class="text-right" style="margin-top:120px;">
						{{ link_to('usuario', 'Cancelar', ['class' => 'btn btn-warning']) }}
						{!!Form::submit('Guardar', ['class' => 'btn btn-success',  'type' => 'submit', 'id' => 'guardar'] ) !!}
					</div>
					{{ csrf_field() }}
					{{ Form::close() }}

				</div>
			</div>
		</section>
	</div>

	@stop