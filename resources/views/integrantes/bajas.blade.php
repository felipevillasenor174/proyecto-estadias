@extends('layouts.app')

@section('content')
<div class="col-sm-5">


	<div class="card text-center" style="width: 18rem;margin-top: 50px">
	<img class="card-img-top rounded-circle mx-auto d-block" src="/images/{{ $Integrante->img_url }}" alt="" style="height: 100px; background-color:#EFE; margin: 30px">
	
	<div class="card-body">
		<h5 class="card-title">{{$Integrante->nombre}}</h5>
		<p>{{$Integrante->email}}
		</p>

		
	</div>
</div>
<form method="POST" action="/integrantes/{{$Integrante->nombre}}">
	{{method_field('DELETE')}}
	{{ csrf_field() }}

	<button type="submit" class="btn btn-success">Eliminar</button>
</form>
</div>
@endsection