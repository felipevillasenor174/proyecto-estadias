@extends('layouts.app')

@section('content')
<div class="container mt-3">
	<form class="form-group" method="POST" action="/integrantes/{{$integrante->nombre}}" enctype="multipart/form-data">
		{{ csrf_field() }}

		{{method_field('put')}}
		
		<div class="form-group">
			
			<label for="">nombre</label>
			<input type="text" class="form-control" name="nombre" value="{{$integrante->nombre}}">
		</div>
		<div class="form-group">

			<label for="">Email</label>
			<input type="text" name="email" class="form-control" value="{{$integrante->email}}">
		</div>
		<div class="form-group">
			<label for="">Avatar</label>
			<input type="file" name="avatar">
		</div>
		<br>
		<button type="submit" class="btn btn-success">actualizar</button>
	</form>
@endsection