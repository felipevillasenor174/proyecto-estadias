<?php

namespace estadia;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
       	protected $primaryKey='id_servicio';
    protected $fillable=['nombre', 'posicion', 'img_url'];

 

 /**
 * Get the route key for the model.
 *
 * @return string
 */
public function getRouteKeyName()
{
    return 'nombre' ;
 }
}
