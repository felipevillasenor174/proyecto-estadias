<?php

namespace estadia\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use estadia\User;

class RolesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $roles)
    {
     if(Auth::user())
     {
        $rol_id=Auth::user()->id_rol;
        $roles = explode("-", $roles);
        if(in_array($rol_id, $roles) == false) 
        {
            return Redirect::to('/');
        }
    }
    else
    {
      return Redirect::to('/');
  }

  return $next($request);
}
}
