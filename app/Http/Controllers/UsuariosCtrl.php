<?php

namespace estadia\Http\Controllers;

use Illuminate\Http\Request as Req;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use estadia\User;
use estadia\Role;
use Request;
use Hash;
use Pluck;

class UsuariosCtrl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     $us=User::all();
     return view('plataforma.usuarios.index', compact('us'));
   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     $datos = new User();
     $rol=Role::all()->Pluck('nombre', 'id_rol');
       // $pos = [];
       // for ($i=1; $i <= count(Role::all()) ; $i++) {
       //  $pos[$i] = $i;
    // }
     $data = [
      'us2' => $datos,
        // 'posi' => $pos
    ];
      // return $data;
    return view('plataforma.usuarios.save', compact('rol'))->with($data);
  }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response5tbn
     */
    public function store(Req $request)
    {
      $inputs = Request::all();
      $usc = new User();
      $rules = [
        'name' => 'required|min:4|alpha',
        'email' => 'required|email|unique:users,email',
        'password' => 'required|min:4',
      ];
      $messages = [
        'name.required' => 'Creo que olvidaste escribir en este campo',
        'email.required' => 'Creo que olvidaste escribir en este campo',
        'name.min' => 'Debes completar con al menos 4 caracteres',
        'name.alpha' => 'Debes contener solo texto',
        'email.unique' => 'El correo ya se encuentra registrado',
        'email.email' => 'El correo esta mal escrito',
        'password.min' => 'Debes completar con al menos 4 caracteres',
        'password.required' => 'Creo que olvidaste escribir en este campo',
        
      ];
      $validar = Validator::make($inputs, $rules, $messages);
      Request::flash();
      if($validar->fails()){
        return Redirect::back()->withInput(Request::all())->withErrors($validar);
      }
      $usc->name=$request->input('name');
      $usc->email=$request->input('email');
    // $usc->password=$request->input('password');
      $usc->password=Hash::make($inputs['password']);
      $usc->id_rol=$request->input('id_rol');
      $usc->save();
      return redirect()->route('usuario'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     $rol=Role::all()->Pluck('nombre', 'id_rol');
        //return view('plataforma.usuarios.save', compact('us2'));
     $datos = User::findOrFail($id);
      // $pos = [];
      // for ($i=1; $i <= count(User::all()) ; $i++) {
      //   $pos[$i] = $i;
      // }
     $data = [
      'us2' => $datos,
        // 'posiciones' => $pos
    ];
      // return $data;
    return view('plataforma.usuarios.save', compact('rol'))->with($data);
  }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Req $request, $id)
    {
      $us=User::all();
      
     //$inputs = Request::all();
      $inputs = Request::all();
      $usa = User::findOrFail($id);
      $dataBanner = [
        'name' => $request['name'],
        'email' => $request['email'],
        'password' => Hash::make($inputs['password']),
        'id_rol' => $request['id_rol']

      ];
      $rules = [
        'name' => 'required|min:4|alpha',
        'email' => 'required|email',
        'password' => 'required|min:4',
      ];
      $messages = [
        'name.required' => 'Creo que olvidaste escribir en este campo',
        'email.required' => 'Creo que olvidaste escribir en este campo',
        'name.min' => 'Debes completar con al menos 4 caracteres',
        'name.alpha' => 'Debes contener solo texto',
        'email.email' => 'El correo esta mal escrito',
        'password.min' => 'Debes completar con al menos 4 caracteres',
        'password.required' => 'Creo que olvidaste escribir en este campo',
        
      ];
      $validar = Validator::make($inputs, $rules, $messages);
      Request::flash();
      if($validar->fails()){
        return Redirect::back()->withInput(Request::all())->withErrors($validar);
      }
      $usa->fill($dataBanner)->save();

            // session()->flash('success','Banner actualizado!');
      return redirect()->route('usuario');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      User::destroy($id);
      return redirect()->route('usuario');
    }
  }
