<?php

namespace estadia\Http\Controllers;

use Illuminate\Http\Request;
use estadia\Http\Request\StoreTrainerRequest;
use estadia\Servicio;

class ServicioCon extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $servicios=Servicio::all();
        return view('servicios.index', compact('servicios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    return view('servicios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          if ($request->hasFile('avatar')) {
         $file= $request->file('avatar');
         $name =time().$file->getClientOriginalName(); 
         $file->move(public_path().'/images/', $name);
        }
        $servicios = new Servicio();
        $servicios->nombre=$request->input('nombre');
         $servicios->posicion=$request->input('posicion');
        $servicios->img_url=$name;
        $servicios->save();
        return 'guardado';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Servicio $servicio)
    {
        return view('servicios.bajas', compact('servicio'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Servicio $servicio)
    {
        return view('servicios.edit', compact('servicio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Servicio $servicio)
    {
        $servicio->fill($request->except('avatar'));
       if ($request->hasFile('avatar')) {
         $file= $request->file('avatar');
         $name =time().$file->getClientOriginalName(); 
         $servicio->img_url = $name;
         $file->move(public_path().'/images/', $name);
        }
        $servicio->save();
         return redirect()->route('servicios.index')->with('status', 'Se actualizo a el servicio '  );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Servicio $servicio)
    {
         $file_path=public_path().'/images/'.$servicio->img_url;
        \File::delete($file_path);
     $servicio->delete();
     return redirect()->route('servicios.index')->with('status', 'Se elimino a el servicio');
    }
}
