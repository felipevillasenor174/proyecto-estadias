<?php

namespace estadia\Http\Controllers;

use Illuminate\Http\Request;
use estadia\Http\Request\StoreTrainerRequest;
use estadia\Integrante;

class IntegranteCon extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $integrante=Integrante::all();
        return view('integrantes.index', compact('integrante'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('integrantes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
             if ($request->hasFile('avatar')) {
         $file= $request->file('avatar');
         $name =time().$file->getClientOriginalName(); 
         $file->move(public_path().'/images/', $name);
        }
        $Integrantes = new Integrante();
        $Integrantes->nombre=$request->input('nombre');
        $Integrantes->email=$request->input('email');
        $Integrantes->img_url=$name;
        $Integrantes->save();
        return 'guardado';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Integrante $Integrante)
    {
         return view('integrantes.bajas', compact('Integrante'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Integrante $integrante)
    {
        return view('integrantes.edit', compact('integrante'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Integrante $integrante)
    {
       $integrante->fill($request->except('avatar'));
       if ($request->hasFile('avatar')) {
         $file= $request->file('avatar');
         $name =time().$file->getClientOriginalName(); 
         $integrante->img_url = $name;
         $file->move(public_path().'/images/', $name);
        }
        $integrante->save();
         return redirect()->route('integrantes.index')->with('status', 'Se actualizo a el integrante '  );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Integrante $Integrante)
    {
         $file_path=public_path().'/images/'.$Integrante->img_url;
        \File::delete($file_path);
     $Integrante->delete();
     return redirect()->route('integrantes.index')->with('status', 'Se elimino a el integrante');
    }
}
