<?php

namespace estadia;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable{
    use Notifiable;

    protected $table = 'users';
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'id_rol', 'imagen',
    ];
    
    public function getRouteKeyName()
{
    return 'id';
} 


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function rol(){
      return $this->hasOne('estadia\Role','id_rol','id_rol');
    }

    public function cotizaciones(){
      return $this->hasMany('estadia\Cotizaciones', 'id', 'id_usuario');
    }

    public function seguimientos(){
      return $this->belongsTo('estadia\Seguimiento','id', 'id_usuario');
    }
}
