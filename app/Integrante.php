<?php

namespace estadia;

use Illuminate\Database\Eloquent\Model;

class Integrante extends Model
{
    	protected $primaryKey='id_integrante';
    protected $fillable=['nombre', 'email', 'img_url'];

 

 /**
 * Get the route key for the model.
 *
 * @return string
 */
public function getRouteKeyName()
{
    return 'nombre' ;
 }
}
