<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'dashboardCrtl@index')->name('inicio');
Route::group(['middleware' => 'roles:1-2', 'prefix' => '/'], function () {

    // Aqui se registran los módulos dentro de le plataforma. Ejemplo:
	Route::resource('usuario', 'UsuariosCtrl'); 
	Route::get('/usuario', 'UsuariosCtrl@index')->name('usuario');
});
//Route::resource('plataforma', 'BannerCtrl');

Route::resource('integrantes', 'IntegranteCon');
Route::resource('servicios', 'ServicioCon');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
